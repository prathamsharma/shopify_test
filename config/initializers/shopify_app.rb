ShopifyApp.configure do |config|
  config.application_name = "My Shopify App"
  config.api_key = "fec31635058397a91137d2ba83f2ce3b"
  config.secret = "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"
  config.old_secret = "<old_secret>"
  config.scope = "read_products, write_orders" # Consult this page for more scope options:
                                 # https://help.shopify.com/en/api/getting-started/authentication/oauth/scopes
  config.embedded_app = true
  config.after_authenticate_job = false
  config.api_version = "2019-10"
  config.session_repository = Shop
end
